﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Productivity.Gateway.Repository.Models
{
    public class Document
    {
        public int DocumentId { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
    }
}
